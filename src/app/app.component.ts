import { Component, OnInit } from '@angular/core';
import { VSBusinessObject } from 'vset-mainmod';
import { AuthGuard } from './core/guards/auth.guard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor(private authGuard: AuthGuard){}
  ngOnInit() {
    VSBusinessObject.callRequiresAuthentication = () => {
      this.authGuard.testValidUser();
    }
  }
}
