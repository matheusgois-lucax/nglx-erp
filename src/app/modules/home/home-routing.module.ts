import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./components/home.component";
import { NgModule } from "@angular/core";

const routesHome: Routes = [
    { path: '', component: HomeComponent, data: { tabOptions: { title: 'Home' } } }
];

@NgModule({
    imports: [RouterModule.forChild(routesHome)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
