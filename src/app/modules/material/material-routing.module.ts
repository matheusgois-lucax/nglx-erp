import { Routes, RouterModule } from "@angular/router";
import { MaterialComponent } from "./components/material.component";
import { NgModule } from "@angular/core";

const routesMaterial: Routes = [
    { path: '', component: MaterialComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routesMaterial)],
    exports: [RouterModule]
})
export class MaterialRoutingModule { }
