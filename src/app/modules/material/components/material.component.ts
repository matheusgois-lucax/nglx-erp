import { Component, OnInit } from '@angular/core';
import { ELXBusinessOrganizationDD, ELXBusinessUnitDD, ELXCompanyDD } from '../../../../ngdx/vset/lucax_erplucax';

@Component({
  selector: 'material',
  templateUrl: './material.component.html'
})
export class MaterialComponent implements OnInit {

  organizationFilterItems: any;
  categoryFilterItems = ['Categoria 1', 'Categoria 2', 'Categoria 3'];
  materialFilterItems = ['Material 1', 'Material 2', 'Material 3'];
  businessUnitFilterItems: any;
  companyFilterItems: any;
  divisionFilterItems = ['Divisão 1', 'Divisão 2', 'Divisão 3'];

  constructor() { 

  }

  ngOnInit() {
    ELXBusinessOrganizationDD.populate('a.Id', 'Description').then((out) => { this.organizationFilterItems = out; });
    ELXBusinessUnitDD.populate('a.Id', 'Description').then((out) => { this.businessUnitFilterItems = out; });
    ELXCompanyDD.populate('a.Id', 'Description').then((out) => { this.companyFilterItems = out; });
    
  }

}
