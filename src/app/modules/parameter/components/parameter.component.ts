import { Component, OnInit, ViewChild } from '@angular/core';
import { DragulaService } from 'ng2-dragula';

//DRAGULA IMPORTS
import * as dragula from 'dragula';
import { ELXMarketSegmentDAO, ELXPackageTransactionDAO, ELXPackageTransactionVersionDAO, ELXModuleDAO, ELXPackageModuleVersionDAO, ELXModuleDD, ELXTransactionDAO, ELXTransactionDD } from '../../../../ngdx/vset/lucax_erplucax';
import { DxTreeViewComponent, DxListComponent } from 'devextreme-angular';
import DataSource from 'devextreme/data/data_source';
import { ELXModuleDAOStore } from '../../../../ngdx/components/elxmoduledao.component';
import { ELXTransactionDAOStore } from '../../../../ngdx/components/elxtransactiondao.component';

@Component({
  selector: 'parameter',
  templateUrl: './parameter.component.html'
})
export class ParameterComponent implements OnInit {
  @ViewChild(DxTreeViewComponent) treeViewChild: DxTreeViewComponent;
  @ViewChild(DxListComponent) listChild: DxListComponent;


  //DRAGULA OPTIONS
  drake = dragula({
    isContainer: function (el) {
      return false; // only elements in drake.containers will be taken into account
    },
    moves: function (el, source, handle, sibling) {
      console.log("handle", handle);
      if (el.parentElement.className.indexOf("treeItemDrop") != -1) {
        console.log('moves');
        return false;
      }
      else
        return true;
    },
    accepts: function (el, target, source, sibling) {
      if (target.className.indexOf("treeItemDrop") != -1)
        return true; // elements can be dropped in any of the `containers` by default
      else
        return false;
    },
    invalid: function (el, handle) {
      return false; // don't prevent any drags from initiating by default
    },
    direction: 'horizontal',        // Y axis is considered when determining where an element would be dropped
    copy: true,                     // elements are moved by default, not copied
    revertOnSpill: false,            // spilling will put the element back where it was dragged from, if this is true
    removeOnSpill: false,            // spilling will `.remove` the element, if this is true
    mirrorContainer: document.body, // set the element that gets mirror elements appended
    ignoreInputTextSelection: true  // allows users to select input text, see details below
  });

  buttonOptions: any = {
    text: "Salvar",
    type: "success",
    useSubmitBehavior: true
  }
  items: Array<Object> = [];
  currentItemForm: CurrentItemForm = new CurrentItemForm();
  contextItems: any;
  titleTransaction = 'Transações';
  titleModule = 'Módulos';
  listModuleItems: DataSource = new DataSource(new ELXModuleDAOStore());
  listTransactionItems: DataSource = new DataSource(new ELXTransactionDAOStore());
  currentClickItemContext: any;
  titleForm: string;
  titleRepo: string;
  itemCurrentSelect: any;
  iNode: number = 0;
  packageTransactionDAO: ELXPackageTransactionDAO = new ELXPackageTransactionDAO();
  versionpkgTransactionDAO: ELXPackageTransactionVersionDAO = new ELXPackageTransactionVersionDAO();
  pkgModuleVersionDAO: ELXPackageModuleVersionDAO = new ELXPackageModuleVersionDAO();
  currentTag: number = 0;
  moduleDAO = new ELXModuleDAO;
  modules: any = [];
  transaction: any = [];
  listItems: any;
  repoEnable: boolean = false;

  constructor(private dragulaService: DragulaService) {

    //DRAGULA BIND
    dragulaService.add('dragDropBag', this.drake);

    dragulaService.drag.subscribe((el, source) => {
      console.log(`drag:`, el);
      console.log(`dragSource -> `, source)
    });
    dragulaService.over.subscribe((el, container, source) => {
      var divHighlight = el.find(this.findOverTreeElement);
      divHighlight.classList.add("dx-state-hover");
      console.log('over ', el)
    });
    dragulaService.out.subscribe((el, container, source) => {
      var divExtinguish = el.find(this.findOverTreeElement);
      divExtinguish.classList.remove("dx-state-hover");
      console.log('out ', divExtinguish)
    });

    dragulaService.drop.subscribe((el, target, source, sibling) => {
      this.drake.cancel(true);
      let itemDrop: string = el[2].innerText;
      let treeViewItemsToDrop: any = this.treeViewChild.items;
      let dropCurrent: any;

      for (const i of treeViewItemsToDrop) { //pega o objeto do item dropado
        if (itemDrop.trim() == i.namenode.trim()) {
          dropCurrent = i;
        }
      }

      console.log('id ', itemDrop);
      console.log('drop ', dropCurrent)
      console.log('idmodule ', this.itemCurrentSelect)

      if (dropCurrent.tag == 2) {    //verifica se esta dropando nas versoes
        this.insert(ELXPackageModuleVersionDAO,
          null,
          null,
          null,
          null,
          null,
          dropCurrent.id,
          this.itemCurrentSelect.key
        );
      }
      else {
        alert('Só é possivel adicionar itens nas Versões de Pacote para Transação.')
      }


    });
    dragulaService.cloned.subscribe((clone, original, type) => {
      console.log(`cloned:`, clone);
      console.log(`cloned:`, original);
      console.log(`cloned:`, type);
    });

    //END DRAGULA BIND

    this.titleForm = "Detalhes";


    this.contextItems = [{
      id: 1,
      text: 'Incluir',
      icon: 'add'
    }, {
      id: 2,
      text: 'Excluir',
      icon: 'trash'
    }];

  }

  ngOnInit() {


    //carrega modules
    ELXModuleDD.populate('id', 'name').then((data) => {
      this.modules = data;
    }, (err) => {
      console.log('err ', err)
    })

    //carrega transacoes
    ELXTransactionDD.populate('id', 'name').then((data) => {
      this.transaction = data;
    }, (err) => {
      console.log('err ', err)
    })

    //LIST ITEMS
    let out: Object;
    new ELXMarketSegmentDAO().list().then((success) => {
      for (const obj of success) {
        out = new Object;
        out['idnode'] = ++this.iNode;
        out['namenode'] = obj.name;
        out['tag'] = 0;
        out['nameTag'] = 'Segmento';
        out['nextNameTag'] = 'Pacote Transação';
        for (let prop in obj) {
          out[prop] = obj[prop];
        }
        out['children'] = [];
        this.items.push(out);
      }
    }, (code) => {
      console.log('code ', code)
    })
    // this.init();

  }
  /* GUIDE PARENTID
   0 - SEGMENTO ~
    1 - PACOTE TRANSACTION ~
      2 - VERSION PKG TRANSACTION ~
        3 - PKG MODULE VERSION ~ (mudulos jogados aqui #instancia)
  
 */

  createChildren = (parent) => {
    console.log('child ', parent);

    if (parent != null) {

      if (parent.itemData.tag == 0) {   //PKGTRANSACTION
        return new Promise((resolve, reject) => {
          this.packageTransactionDAO.filter('IdMarketSegment = ' + parent.itemData.id).then((output) => {
            let aux = [];
            for (const obj of output) {
              let out = {};
              out['idnode'] = ++this.iNode;
              out['namenode'] = obj.name;
              out['parentnode'] = parent.itemData.idnode;
              out['tag'] = 1;
              out['nameTag'] = 'Pacote Transação';
              out['nextNameTag'] = 'Versão Pacote Transação';
              for (let prop in obj) {
                out[prop] = obj[prop];
              }
              aux.push(out);
            }
            console.log('success')
            resolve(aux);
          }, (err) => {
            console.log('err')
            resolve([]);
          })
        })
      }

      else if (parent.itemData.tag == 1) {   //VERSION PKG 
        return new Promise((resolve, reject) => {
          this.versionpkgTransactionDAO.filter('IdPackageTransaction = ' + parent.itemData.id).then((output) => {
            let aux = [];
            for (const obj of output) {
              let out = {};
              out['idnode'] = ++this.iNode;
              out['namenode'] = obj.version;
              out['parentnode'] = parent.itemData.idnode;
              out['tag'] = 2;
              out['nameTag'] = 'Versão Pacote';
              out['nextNameTag'] = 'Módulos';
              for (let prop in obj) {
                out[prop] = obj[prop];
              }
              aux.push(out);
            }
            resolve(aux);
          }, (err) => {
            resolve([]);
          })
        })
      }

      else if (parent.itemData.tag == 2) {   //Modulos
        return new Promise((resolve, reject) => {
          this.pkgModuleVersionDAO.filter('IdPackageTransactionVersion = ' + parent.itemData.id).then((output) => {
            let aux = [];

            for (const obj of output) {
              let out = {};
              out['idnode'] = ++this.iNode;
              for (const a of this.modules) {
                if (obj.idModule == a.key) {
                  out['namenode'] = a.value;
                }
              }
              out['parentnode'] = parent.itemData.idnode;
              out['nameTag'] = 'Módulos';
              out['tag'] = 3;
              for (let prop in obj) {
                out[prop] = obj[prop];
              }
              aux.push(out);
            }

            resolve(aux);
          }, (err) => {
            resolve([]);
          })


        })
      }
    }
    else if (this.treeViewChild != undefined) {
      // this.treeViewChild.instance.repaint();
    }

  }


  onItemClickTreeView(e) {
    this.repoEnable = false;
    let tag = e.tag;
    console.log('clicado ', e)
    this.currentItemForm = e;
    console.log('current ')
    this.titleForm = 'Detalhes ' + this.currentItemForm.name;

    if (tag == 2) {
      this.repoEnable = true;
      this.titleRepo = 'Repositório de Módulos'
      this.listItems = this.modules;
    }
    else if (tag == 3) {
      this.repoEnable = true;
      this.titleRepo = 'Repositório de Transações'
      this.listItems = this.transaction;
    }

    if (e.tag == 0)
      tag = 0;
    else if (e.tag == 1)
      tag = 1;

    this.currentTag = tag;
  }

  onItemClickContext(e) {
    this.currentTag = 0;
    console.log('currentClickItemContext ', this.currentClickItemContext)

    if (e.id == 1) {
      if (this.currentClickItemContext != undefined) {
        if (this.currentClickItemContext.tag != 2) { //nao aceita adicionar modulo
          let before = new CurrentItemForm();
          this.currentTag = this.currentClickItemContext.tag;
          before = this.currentClickItemContext;
          this.currentTag++;
          this.titleForm = 'Adicionar ' + this.currentClickItemContext.nextNameTag + ' em ' + this.currentClickItemContext.name;
          this.currentItemForm = new CurrentItemForm();
          this.currentItemForm.idMarketSegment = before.id;
          this.currentItemForm.idPackageTransaction = before.id;
          this.currentClickItemContext = undefined;
        } else {
          alert('Não é possivel adicionar Módulo/Transações. Apenas arrastar.')
          this.currentTag = null;
          this.titleForm = 'Detalhes';
        }
      } else { //cad segment
        this.titleForm = 'Adicionar Segmento';
        this.currentItemForm = new CurrentItemForm();
      }
    } else if (e.id == 2) {  //exluir
      if (this.currentClickItemContext.nameTag == 'Segmento') {
        this.delete(ELXMarketSegmentDAO, this.currentClickItemContext.id);
      }

      else if (this.currentClickItemContext.nameTag == 'Pacote Transação') {
        this.delete(ELXPackageTransactionDAO, this.currentClickItemContext.id);
      }

      else if (this.currentClickItemContext.nameTag == 'Versão Pacote') {
        this.delete(ELXPackageTransactionVersionDAO, this.currentClickItemContext.id);
      }

      else if (this.currentClickItemContext.nameTag == 'Módulos') {
        this.delete(ELXPackageModuleVersionDAO, this.currentClickItemContext.id);
      }

      else if (this.currentClickItemContext.nameTag == 'Transações') {
        this.delete(ELXTransactionDAO, this.currentClickItemContext.id);
      }
    }
  }

  onItemContextMenuTreeView(e) {
    this.currentClickItemContext = e.itemData;
    console.log('currentClickItemContextTree ', this.currentClickItemContext)

  }

  findOverTreeElement(element) {
    if (element == undefined)
      return;
    if (element.className == undefined)
      return;
    if (element.className.indexOf("treeItemDrop") != -1)
      return element;
  }

  onClickHold(e) {
    console.log('click ', e)
    this.itemCurrentSelect = e.itemData;
  }

  onContentReadyObjetos(e) {
    const itemListDrag: any = e.element.getElementsByClassName('dx-list-item'); //eventos que aceitam drag
    for (const item in itemListDrag) {
      if (itemListDrag.hasOwnProperty(item)) {
        const element = itemListDrag[item];
        this.addElementDragDrop(element);
      }
    }


  }

  init(): Array<Object> {
    let out: Object;
    new ELXMarketSegmentDAO().list().then((success) => {
      for (const obj of success) {
        out = new Object;
        out['idnode'] = ++this.iNode;
        out['namenode'] = obj.name;
        out['tag'] = 0;
        out['nameTag'] = 'Segmento';
        out['nextNameTag'] = 'Pacote Transação';
        for (let prop in obj) {
          out[prop] = obj[prop];
        }
        out['children'] = [];
        this.items.push(out);
      }
    }, (code) => {
      console.log('code ', code)
    })
    return this.items;
  }

  onContentReadyTree(e) {
    // console.log('etree ', e)
    e.itemElement.classList.add("treeItemDrop"); //onde o item pode ser dropado
    this.addElementDragDrop(e.itemElement);
    // e.repaint;
  }

  addElementDragDrop(element: any) {
    let drakeBag = this.dragulaService.find('dragDropBag').drake;
    drakeBag.containers.push(element);
  }

  onFormSubmit(e) {
    console.log('submit ', this.currentItemForm)
    console.log('submittag ', this.currentTag)
    if (this.currentTag == 0) {// cad segmento
      if (this.currentItemForm.id == null || this.currentItemForm.id == undefined || this.currentItemForm.id == Number('')) {
        this.insert(ELXMarketSegmentDAO,
          this.currentItemForm.name,
          null,
          null,
          null,
          null,
          null,
          null
        );
      } else {
        this.update(ELXMarketSegmentDAO,
          this.currentItemForm.id,
          this.currentItemForm.name,
          null,
          null,
          null,
          null,
          null,
          null
        );
      }
    }
    else if (this.currentTag == 1) {
      if (this.currentItemForm.id == null || this.currentItemForm.id == undefined || this.currentItemForm.id == Number('')) {   // cad pkgTransaction
        this.insert(ELXPackageTransactionDAO,
          this.currentItemForm.name,
          this.currentItemForm.idMarketSegment,
          this.currentItemForm.description,
          null,
          null,
          null,
          null
        );
      }
      else {
        this.update(ELXPackageTransactionDAO,
          this.currentItemForm.id,
          this.currentItemForm.name,
          this.currentItemForm.idMarketSegment,
          this.currentItemForm.description,
          null,
          null,
          null,
          null
        );
      }
    }

    else if (this.currentTag == 2) {
      if (this.currentItemForm.id == null || this.currentItemForm.id == undefined || this.currentItemForm.id == Number('')) {    // cad version pkgTransaction
        this.insert(ELXPackageTransactionVersionDAO,
          null,
          null,
          null,
          this.currentItemForm.idPackageTransaction,
          this.currentItemForm.version,
          null,
          null
        );
      }
      else {
        this.update(ELXPackageTransactionVersionDAO,
          this.currentItemForm.id,
          null,
          null,
          null,
          this.currentItemForm.idPackageTransaction,
          this.currentItemForm.version,
          null,
          null
        );
      }
    }


    e.preventDefault();
    this.treeViewChild.instance.repaint();

  }

  onItemSelectionChanged(e) {
    console.log('SELECTION', e)
  }

  onFieldDataChanged(e) {
    console.log('changed ', e)
  }

  onItemHold(e) {
    console.log('e', e)
  }

  insert(daoName: any, name?: string, idMarketSegment?: number, description?: string, idPackageTransaction?: number,
    version?: string, idPackageTransactionVersion?: number, idModule?: number, treeview?: DxTreeViewComponent) {
    let daoNameVar = new daoName();

    daoNameVar.input.name = name;
    daoNameVar.input.idMarketSegment = idMarketSegment;
    daoNameVar.input.description = description;
    daoNameVar.input.idPackageTransaction = idPackageTransaction;
    daoNameVar.input.version = version;
    daoNameVar.input.idPackageTransactionVersion = idPackageTransactionVersion;
    daoNameVar.input.idModule = idModule;
    console.log('insert ', daoNameVar.input)
    daoNameVar.insert().then((data) => {
      console.log('input -> ', daoNameVar.input + ' - ' + data);
    }, (err) => {
      console.log('erro insert ', daoNameVar.input);
      console.log(' err = ' + err);
    })
  }

  update(daoName: any, id: number, name?: string, idMarketSegment?: number, description?: string,
    idPackageTransaction?: number, version?: string, idPackageTransactionVersion?: number, idModule?: number) {
    let daoNameVar = new daoName();

    daoNameVar.input.id = id;
    daoNameVar.input.name = name;
    daoNameVar.input.idMarketSegment = idMarketSegment;
    daoNameVar.input.description = description;
    daoNameVar.input.idPackageTransaction = idPackageTransaction;
    daoNameVar.input.version = version;
    daoNameVar.input.idPackageTransactionVersion = idPackageTransactionVersion;
    daoNameVar.input.idModule = idModule;
    daoNameVar.update().then((data) => {
      console.log('update -> ', daoNameVar.input + ' - ' + data);
      // console.log('create ', this.createChildren(this.lastParent));
      this.createChildren(null);
      console.log('saida');

    }, (err) => {
      console.log('erro update ', daoNameVar.input + ' err = ' + err);
    })
  }

  delete(daoName: any, id: number) {
    let daoNameVar = new daoName();

    daoNameVar.input.id = id;
    console.log('delete ', daoNameVar.input)
    daoNameVar.delete().then((data) => {
      console.log('delete -> ', daoNameVar.input + ' - ' + data);
    }, (err) => {
      console.log('erro delete ', daoNameVar.input + ' err = ' + err);
    })
  }

}

export class CurrentItemForm {
  id?: number;
  idMarketSegment?: number;
  idPackageTransaction?: number;
  name?: string;
  lastUpdateDate?: string;
  description?: string;
  version?: string;
}

