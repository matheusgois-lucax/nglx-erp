import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParameterRoutingModule } from './parameter-routing.module';
import { ParameterComponent } from './components/parameter.component';
import { DxTreeViewModule, DxFormModule, DxListModule, DxContextMenuModule } from 'devextreme-angular';
import { DragulaModule } from '../../../../node_modules/ng2-dragula';

@NgModule({
  imports: [
    CommonModule,
    ParameterRoutingModule,
    DxTreeViewModule,
    DxFormModule,
    DxListModule,
    DxContextMenuModule,
    DragulaModule
  ],
  declarations: [ParameterComponent]
})
export class ParameterModule { }
