import { Routes, RouterModule } from "@angular/router";
import { ParameterComponent } from "./components/parameter.component";
import { NgModule } from "../../../../node_modules/@angular/core";

const routesParameter: Routes = [
    { path: '', component: ParameterComponent, data: { tabOptions: { title: 'Home' } } }
];

@NgModule({
    imports: [RouterModule.forChild(routesParameter)],
    exports: [RouterModule]
})
export class ParameterRoutingModule { }
