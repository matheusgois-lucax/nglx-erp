import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { VSConnection } from 'vset-mainmod';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  loginRedirect() {
      this.router.navigateByUrl('/login')
  }
  
  testValidUser():boolean {
    if (!VSConnection.instance().userIsLoggedIn) this.loginRedirect();
    return true;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.testValidUser();
  }
}
