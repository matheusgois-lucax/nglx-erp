export interface VXTabOptionInterface {
    urlPrefix: string; // <-- caso a tab encerre enquanto na DOM, redirecionar através da rota pai
    title: string; // <-- titulo do texto para tab
    category?: Array<string> | string; // <-- cria categorias de tabs, podendo ter várias tabs, caso vazio, a rota pertence a todas as categorias
    icon?: string; // <-- icone para tab
}
