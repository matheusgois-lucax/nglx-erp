import { Injectable, Injector } from '@angular/core';
import { VXCustomReuseStrategy } from './reusestrategy';
import { Router } from '@angular/router';
import { VXTabOptionInterface } from './tab-option.interface';

function isArray (testList: any): boolean {
    // needs this type of test for older browsers
    return typeof testList === 'object' && testList.length !== undefined;
}

export class VXTabModel {
    index: number = 0;
    count: number;
    title: string;
    keyPath: string;
    urlPrefix: string;
    category: Array<string> = [];
    selected: boolean;
    icon: string;

    constructor(private tabService?: VXTabService) {}

    remove() {
        this.tabService.remove(this);
    }

    belongsCategory(category: string) {
        if (this.category.length === 0) return true;
        for (let i = 0; i < this.category.length; i++) {
            if (this.category[i] === category) return true;
        }
        return false;
    }

    domain() {
        return this.urlPrefix + this.keyPath;
    }
}

@Injectable()
export class VXTabService {

    private tabs: Array<VXTabModel> = [];
    private count: number = 0;

    private customStrategy: VXCustomReuseStrategy;
    private router: Router;

    constructor(private injector: Injector) {
        // WORKAROUND - argumentacao: evitar injecoes ciclicas
        setTimeout(() => {
            this.router = this.injector.get(Router);
        }, 10);
    }

    registerCustomStrategy(customStrategy: VXCustomReuseStrategy) {
        this.customStrategy = customStrategy;
    }

    add(keyPath: string, tabOptions: VXTabOptionInterface) {
        const id = Math.floor(Math.random() * 1000);
        for (let i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].keyPath === keyPath) {
                return;
            }
        }
        const tabModel: VXTabModel = new VXTabModel(this);
        tabModel.title = tabOptions.title;
        tabModel.keyPath = keyPath;
        tabModel.category = (tabOptions.category === undefined) ? [] : (isArray(tabOptions.category)) ? tabOptions.category as Array<string> : [tabOptions.category as string];
        tabModel.urlPrefix = tabOptions.urlPrefix;
        tabModel.selected = true;
        tabModel.count = this.count++;
        tabModel.icon = tabOptions.icon;
        this.tabs.push(tabModel);
        this.calculatePosition();
    }

    retriveSideTab(tabModel: VXTabModel): VXTabModel {
        const currPosition = this.tabs.indexOf(tabModel);
        return this.tabs[(currPosition === 0) ? 1 : currPosition - 1];
    }

    remove(tabModel: VXTabModel) {
        if (this.tabs.length === 1) return;
        if (window.location.pathname.indexOf(tabModel.keyPath) !== -1) {
            const navigateToTabModel = this.retriveSideTab(tabModel);
            this.router.navigateByUrl(navigateToTabModel.urlPrefix.concat(navigateToTabModel.keyPath));
            setTimeout(() => {
                this.customStrategy.manuallyDetach(tabModel.keyPath);
                this.popTab(tabModel);
                this.calculatePosition();
            }, 80);
        } else {
            this.popTab(tabModel);
            this.calculatePosition();
        }
    }

    activating(keyPath: string) {
        for (let i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].keyPath === keyPath) {
                this.tabs[i].selected = true;
            } else {
                this.tabs[i].selected = false;
            }
        }
    }

    calculatePosition(): void {
        for (let i = 0; i < this.tabs.length; i++) {
            this.tabs[i].index = i;
        }
    }

    popTab(tabModel: VXTabModel) {
        this.tabs.splice(this.tabs.indexOf(tabModel), 1);
    }

    list(category?: string): Array<VXTabModel> {
        if (category === undefined || category === '') return this.tabs;
        return this.tabs.filter((tabModel: VXTabModel, index: number) => {
            return tabModel.belongsCategory(category);
        });
    }

    changeTabName(keyPath: string, name: string) {
        for ( let i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].keyPath === keyPath) this.tabs[i].title = name;
        }
    }
}
