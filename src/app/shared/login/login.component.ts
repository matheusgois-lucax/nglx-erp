import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ELXLogin } from '../../../ngdx/vset/lucax_erplucax';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  loginSuccess: boolean; 
  usuario: any;

  constructor(private router: Router) { 

  }

 login(e){
    e.preventDefault();
    const login: ELXLogin = new ELXLogin();
    login.execute('', (success) => {
      this.router.navigate(['erp/home']);
      this.loginSuccess = true;
    }, (code, err) => {
      console.log("error ", err)
      console.log("code ", code)
      alert("ERRO");
    })
  }

  ngOnInit() {
    this.loginSuccess = false;
  }

}
