import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './components/main.component';
import { TreeViewComponent } from '../tree-view/tree-view.component';
import { VXTabsComponent } from '../tabs.component';

//Devextreme
import {
  DxTreeViewModule,
  DxButtonModule,
  DxScrollViewModule,
  DxContextMenuModule
} from 'devextreme-angular';
import { VXTabService } from '../tabs.service';
import { AuthGuard } from '../../core/guards/auth.guard';
import { RouteReuseStrategy } from '@angular/router';
import { VXCustomReuseStrategy } from '../reusestrategy';
import { DndModule } from 'ng2-dnd';


@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    DxTreeViewModule,
    DxButtonModule,
    DxScrollViewModule,
    DxContextMenuModule,
    DndModule.forRoot(),
  ],
  declarations: [
    MainComponent,
    TreeViewComponent,
    VXTabsComponent,
  ],
  providers: [
    VXTabService,
    AuthGuard,
    { provide: RouteReuseStrategy, useClass: VXCustomReuseStrategy, deps: [VXTabService] }
  ],
})
export class MainModule { }
