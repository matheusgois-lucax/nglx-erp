import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'main',
  templateUrl: './main.component.html'
})

export class MainComponent implements OnInit {
  act: boolean = false;
  rotate: boolean = false;

  constructor() { }

  ngOnInit() {
    
  }

  onClick() {
    this.act = !this.act;
    this.rotate = !this.rotate;
  }

}
