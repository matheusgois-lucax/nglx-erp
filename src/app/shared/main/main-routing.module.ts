import { MainComponent } from "./components/main.component";
import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

//itens da treeview tem que ser importados aqui para rota
const routes: Routes = [
    {
        path: '' , component: MainComponent, children: [
            { path: 'home', loadChildren: '../../modules/home/home.module#HomeModule', data: { tabOptions: { title: 'Home' } } },
            { path: 'parameter', loadChildren: '../../modules/parameter/parameter.module#ParameterModule', data: { tabOptions: { title: 'Home' } } },
            { path: 'material', loadChildren: '../../modules/material/material.module#MaterialModule', data: { tabOptions: { title: 'Home' } } },
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainRoutingModule { }
