import { Component } from '@angular/core';
import { VXTabService } from './tabs.service';
import { Input } from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'vx-tabs',
    templateUrl: './tabs.component.html',
})
export class VXTabsComponent {
    @Input() group: string;
    constructor(private tabsService: VXTabService) {}
    getTabs() {
        return this.tabsService.list();
    }
}
