import { Component, OnInit, ViewChild } from '@angular/core';
import { DxTreeViewComponent } from 'devextreme-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'tree-view',
  templateUrl: './tree-view.component.html'
})
export class TreeViewComponent implements OnInit {
  @ViewChild(DxTreeViewComponent) treeView: DxTreeViewComponent;

  contextMenu: any;
  treeViewItems: Array<Object> = [];

  constructor(private router: Router) {
  }

  ngOnInit() {

    this.treeViewItems = [{
      id: 1,
      name: 'Cadastros',
      items: [{
        id: 2,
        name: 'Cadastros do Sistema',
        items: [
        ]
      }, {
        id: 3,
        name: 'Cadastros de Materiais e Serviços',
        items: [
        ]
      }, {
        id: 4,
        name: 'Cadastros Fiscais',
        items: []
      }]
    }, {
      id: 5,
      name: 'Parametrizador',
      rout: 'parameter',
      items: []
    }];

    this.contextMenu = [{
      text: 'Adicionar Favorito',
      icon: 'add',
      // items: itemsArray
    }, {
      text: 'Excluir Favorito',
      icon: 'trash'
    }, {
      text: 'Abrir Aba',
      icon: 'far fa-plus-square'
    }];

  }


  //event for treeview
  selectItem(e) {
    if (!(e.itemData.rout == undefined)) {
      let temp: string = '/erp/' + e.itemData.rout;
      this.router.navigate([temp]);
    }
  }

  //contextmenu
  itemContextSelect(e) {
    if (!(e.itemData.register == undefined)) {
      let temp: string = '/erp/' + e.itemData.register;
      // this.router.navigate([temp]);
    }
  }

  openContextMenu(e) {
  }

}
