import {
    DetachedRouteHandle,
    RouteReuseStrategy,
    ActivatedRouteSnapshot,
    Router} from '@angular/router';
import { Injectable } from '@angular/core';
import { VXTabService } from './tabs.service';
import { VXTabOptionInterface } from './tab-option.interface';


function cookPath (rawPath: string, params: object): string {
    for (const prm in params) {
        if (params.hasOwnProperty(prm)) {
            rawPath = rawPath.replace(':'.concat(prm), params[prm]);
        }
    }
    return rawPath;
}

// This impl. bases upon one that can be found in the router's test cases.
export class VXCustomReuseStrategy implements RouteReuseStrategy {

    handlers: {[key: string]: DetachedRouteHandle} = {};
    constructor (private tabsService: VXTabService) {
        tabsService.registerCustomStrategy(this);
    }

    manuallyDetach(keyPath: string): boolean {
        if (!this.handlers.hasOwnProperty(keyPath)) {
            return false;
        }
        this.handlers[keyPath] = null;
        return true;
    }

    // default: return false;
    /** Determines if this route (and its subtree) should be detached to be reused later */
    shouldDetach(route: ActivatedRouteSnapshot): boolean {
        // console.log('1 ===> shouldDetach', true, route);
        const tabOptions: VXTabOptionInterface = route.data.tabOptions;
        return !!tabOptions;
    }

    // default: {void}
    /**Stores the detached route.
     * Storing a `null` value should erase the previously stored value.
     */
    store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
        // console.log('2 ===> store', route, handle);
        const cookedPath: string = cookPath(route.routeConfig.path, route.params);
        this.handlers[cookedPath] = handle;
    }

    // default: return false;
    /** Determines if this route (and its subtree) should be reattached */
    shouldAttach(route: ActivatedRouteSnapshot): boolean {
        const cookedPath: string = cookPath(route.routeConfig.path, route.params);
        // console.log('3 ===> shouldAttach', !!route.data.useTab && !!route.routeConfig && !!this.handlers[cookedPath], route);
        const tabOptions: VXTabOptionInterface = route.data.tabOptions;
        if (!!tabOptions) {
            tabOptions.urlPrefix = `/${cookPath(route.parent.routeConfig.path, route.parent.params)}/`;
            this.tabsService.add(cookedPath, tabOptions);
        }
        return !!tabOptions && !!route.routeConfig && !!this.handlers[cookedPath];
    }

    // default: return null;
    /** Retrieves the previously stored route */
    retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
        // console.log('4 ===> retrieve', route);
        if (!route.routeConfig) {
            return null;
        }
        const cookedPath: string = cookPath(route.routeConfig.path, route.params);
        this.tabsService.activating(cookedPath);
        return this.handlers[cookedPath];
    }

    // default: same!
    /** Determines if a route should be reused */
    shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
        // console.log('5 ===> shouldReuseRoute', future.routeConfig === curr.routeConfig, future, curr);
        const tabOptions: VXTabOptionInterface = curr.data.tabOptions;
        if (!!tabOptions) {
            // tslint:disable-next-line:max-line-length
            return future.routeConfig === curr.routeConfig && cookPath(future.routeConfig.path, future.params) === cookPath(curr.routeConfig.path, curr.params);
        }
        return future.routeConfig === curr.routeConfig;
    }

}
