import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { LoginComponent } from "./shared/login/login.component";
import { AuthGuard } from "./core/guards/auth.guard";


//importar todos os modulos
export const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'erp', canActivate: [AuthGuard], loadChildren: './shared/main/main.module#MainModule' },
    { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, { enableTracing: false })],
    exports: [RouterModule]
})

export class AppRoutingModule { }