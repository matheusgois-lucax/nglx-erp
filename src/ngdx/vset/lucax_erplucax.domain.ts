/* tslint:disabled */
// NOT DELETE !!!!!!!!!!!!!
export const TDomains: any = {};
// NOT DELETE !!!!!!!!!!!!!



TDomains.ELXLocation_INTERNAL = 'Interno';
TDomains.ELXLocation_EXTERNAL = 'Externo';
TDomains.ELXStatus_ACTIVE = 'Ativo';
TDomains.ELXStatus_BLOCKED = 'Bloqueado';
TDomains.ELXStatus_CANCELED = 'Cancelado';
TDomains.ELXStatus_SUSPENDED = 'Suspenso';
TDomains.ELXOrigin_NATIONAL = 'Nacional';
TDomains.ELXOrigin_EXTERIOR = 'Exterior';
TDomains.ELXGender_MALE = 'Masculino';
TDomains.ELXGender_FEMALE = 'Feminino';
TDomains.ELXMaritalStatus_NEVER_MARRIED = 'Solteiro(a)';
TDomains.ELXMaritalStatus_MARRIED = 'Casado(a)';
TDomains.ELXMaritalStatus_SEPARATED = 'Separado(a)';
TDomains.ELXMaritalStatus_DIVORCED = 'Divorciado(a)';
TDomains.ELXMaritalStatus_WIDOWED = 'Viúvo(a)';
TDomains.ELXMaritalStatus_NOT_STATED = 'Não indicado';
