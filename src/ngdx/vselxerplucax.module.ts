import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DevExtremeModule } from 'devextreme-angular';

// GENERATED COMPONENTS
import { VSELXErplucaxComponent } from './vselxerplucax.component';
import { ELXPackageModuleTransactionVersionComponent } from './components/elxpackagemoduletransactionversiondao.component';
import { ELXPackageModuleVersionComponent } from './components/elxpackagemoduleversiondao.component';
import { ELXPackageTransactionComponent } from './components/elxpackagetransactiondao.component';
import { ELXPackageTransactionVersionComponent } from './components/elxpackagetransactionversiondao.component';
import { ELXRoleComponent } from './components/elxroledao.component';
import { ELXTransactionComponent } from './components/elxtransactiondao.component';
import { ELXBusinessOrganizationComponent } from './components/elxbusinessorganizationdao.component';
import { ELXBusinessOrganizationInfoComponent } from './components/elxbusinessorganizationinfodao.component';
import { ELXLanguageComponent } from './components/elxlanguagedao.component';
import { ELXMarketSegmentComponent } from './components/elxmarketsegmentdao.component';
import { ELXMarketSegmentInfoComponent } from './components/elxmarketsegmentinfodao.component';
import { ELXModuleComponent } from './components/elxmoduledao.component';
import { ELXModuleInfoComponent } from './components/elxmoduleinfodao.component';
import { ELXUniverseComponent } from './components/elxuniversedao.component';

export const routesELXERPLUCAX: Routes = [
    {path: 'vsdao', component: VSELXErplucaxComponent, children: [
        {path: 'elxpackagemoduletransactionversion', component: ELXPackageModuleTransactionVersionComponent}, 
        {path: 'elxpackagemoduleversion', component: ELXPackageModuleVersionComponent}, 
        {path: 'elxpackagetransaction', component: ELXPackageTransactionComponent}, 
        {path: 'elxpackagetransactionversion', component: ELXPackageTransactionVersionComponent}, 
        {path: 'elxrole', component: ELXRoleComponent}, 
        {path: 'elxtransaction', component: ELXTransactionComponent}, 
        {path: 'elxbusinessorganization', component: ELXBusinessOrganizationComponent}, 
        {path: 'elxbusinessorganizationinfo', component: ELXBusinessOrganizationInfoComponent}, 
        {path: 'elxlanguage', component: ELXLanguageComponent}, 
        {path: 'elxmarketsegment', component: ELXMarketSegmentComponent}, 
        {path: 'elxmarketsegmentinfo', component: ELXMarketSegmentInfoComponent}, 
        {path: 'elxmodule', component: ELXModuleComponent}, 
        {path: 'elxmoduleinfo', component: ELXModuleInfoComponent}, 
        {path: 'elxuniverse', component: ELXUniverseComponent} 
    ]}
];

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        DevExtremeModule
    ],
    declarations: [
        VSELXErplucaxComponent, 
        ELXPackageModuleTransactionVersionComponent, 
        ELXPackageModuleVersionComponent, 
        ELXPackageTransactionComponent, 
        ELXPackageTransactionVersionComponent, 
        ELXRoleComponent, 
        ELXTransactionComponent, 
        ELXBusinessOrganizationComponent, 
        ELXBusinessOrganizationInfoComponent, 
        ELXLanguageComponent, 
        ELXMarketSegmentComponent, 
        ELXMarketSegmentInfoComponent, 
        ELXModuleComponent, 
        ELXModuleInfoComponent, 
        ELXUniverseComponent 
    ],
    exports: [
        ELXPackageModuleTransactionVersionComponent, 
        ELXPackageModuleVersionComponent, 
        ELXPackageTransactionComponent, 
        ELXPackageTransactionVersionComponent, 
        ELXRoleComponent, 
        ELXTransactionComponent, 
        ELXBusinessOrganizationComponent, 
        ELXBusinessOrganizationInfoComponent, 
        ELXLanguageComponent, 
        ELXMarketSegmentComponent, 
        ELXMarketSegmentInfoComponent, 
        ELXModuleComponent, 
        ELXModuleInfoComponent, 
        ELXUniverseComponent 
    ]
})
export class VSELXErplucaxModule{ }
