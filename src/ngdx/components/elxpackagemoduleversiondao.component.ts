/**
 * Component store DX: ELXPackageModuleVersionDAOStore
 * Description:  CRUD tabela - PackageModuleVersion
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXPackageModuleVersion, ELXPackageModuleVersionDAO, ELXPackageTransactionVersionDD, ELXModuleDD} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXPackageModuleVersionDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXPackageModuleVersionDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxpackagemoduleversion-dao-component',
    templateUrl: './ELXpackagemoduleversiondao.component.html'
})
export class ELXPackageModuleVersionComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    domainIdPackageTransactionVersion = new VSDomainDataStore(ELXPackageTransactionVersionDD, '<fill key>', '<fill value>');
    domainIdModule = new VSDomainDataStore(ELXModuleDD, '<fill key>', '<fill value>');
    dataSource: DataSource = new DataSource(new ELXPackageModuleVersionDAOStore());
    domains = [this.domainIdPackageTransactionVersion, this.domainIdModule];

    ngOnInit() {
        this.initialize();
    }
}
