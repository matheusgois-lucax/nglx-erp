/**
 * Component store DX: ELXPackageTransactionVersionDAOStore
 * Description:  CRUD tabela - PackageTransactionVersion
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXPackageTransactionVersion, ELXPackageTransactionVersionDAO, ELXPackageTransactionDD} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXPackageTransactionVersionDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXPackageTransactionVersionDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxpackagetransactionversion-dao-component',
    templateUrl: './ELXpackagetransactionversiondao.component.html'
})
export class ELXPackageTransactionVersionComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    domainIdPackageTransaction = new VSDomainDataStore(ELXPackageTransactionDD, 'id', 'name');
    dataSource: DataSource = new DataSource(new ELXPackageTransactionVersionDAOStore());
    domains = [this.domainIdPackageTransaction];

    ngOnInit() {
        this.initialize();
    }
}
