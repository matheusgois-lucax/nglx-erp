/**
 * Component store DX: ELXPackageModuleTransactionVersionDAOStore
 * Description:  CRUD tabela - PackageModuleTransactionVersion
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXPackageModuleTransactionVersion, ELXPackageModuleTransactionVersionDAO, ELXPackageModuleVersionDD, ELXTransactionDD} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXPackageModuleTransactionVersionDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXPackageModuleTransactionVersionDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxpackagemoduletransactionversion-dao-component',
    templateUrl: './ELXpackagemoduletransactionversiondao.component.html'
})
export class ELXPackageModuleTransactionVersionComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    domainIdPackageModuleVersion = new VSDomainDataStore(ELXPackageModuleVersionDD, '<fill key>', '<fill value>');
    domainIdTransaction = new VSDomainDataStore(ELXTransactionDD, '<fill key>', '<fill value>');
    dataSource: DataSource = new DataSource(new ELXPackageModuleTransactionVersionDAOStore());
    domains = [this.domainIdPackageModuleVersion, this.domainIdTransaction];

    ngOnInit() {
        this.initialize();
    }
}
