/**
 * Component store DX: ELXBusinessOrganizationDAOStore
 * Description:  CRUD tabela - BusinessOrganization
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { ELXBusinessOrganizationDAO, ELXUniverseDD} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';
import { DxDataGridComponent } from 'devextreme-angular';

export class ELXBusinessOrganizationDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXBusinessOrganizationDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxbusinessorganization-dao-component',
    templateUrl: './ELXbusinessorganizationdao.component.html'
})
export class ELXBusinessOrganizationComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    domainIdUniverse = new VSDomainDataStore(ELXUniverseDD, '<fill key>', '<fill value>');
    dataSource: DataSource = new DataSource(new ELXBusinessOrganizationDAOStore());
    domains = [this.domainIdUniverse];

    ngOnInit() {
        this.initialize();
    }
}
