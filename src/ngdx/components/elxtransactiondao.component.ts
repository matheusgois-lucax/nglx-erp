/**
 * Component store DX: ELXTransactionDAOStore
 * Description:  CRUD tabela - Transaction
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXTransaction, ELXTransactionDAO, ELXModuleDD} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXTransactionDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXTransactionDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxtransaction-dao-component',
    templateUrl: './ELXtransactiondao.component.html'
})
export class ELXTransactionComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    domainIdModule = new VSDomainDataStore(ELXModuleDD, 'id', 'name');
    dataSource: DataSource = new DataSource(new ELXTransactionDAOStore());
    domains = [this.domainIdModule];

    ngOnInit() {
        this.initialize();
    }
}
