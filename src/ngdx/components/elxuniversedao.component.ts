/**
 * Component store DX: ELXUniverseDAOStore
 * Description:  CRUD tabela - Universe
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXUniverse, ELXUniverseDAO} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXUniverseDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXUniverseDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxuniverse-dao-component',
    templateUrl: './ELXuniversedao.component.html'
})
export class ELXUniverseComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    dataSource: DataSource = new DataSource(new ELXUniverseDAOStore());

    ngOnInit() {
        this.initialize();
    }
}
