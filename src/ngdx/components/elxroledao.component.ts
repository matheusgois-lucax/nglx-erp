/**
 * Component store DX: ELXRoleDAOStore
 * Description:  CRUD tabela - Role
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXRole, ELXRoleDAO} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXRoleDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXRoleDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxrole-dao-component',
    templateUrl: './ELXroledao.component.html'
})
export class ELXRoleComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    dataSource: DataSource = new DataSource(new ELXRoleDAOStore());

    ngOnInit() {
        this.initialize();
    }
}
