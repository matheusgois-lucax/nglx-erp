/**
 * Component store DX: ELXMarketSegmentDAOStore
 * Description:  CRUD tabela - MarketSegment
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXMarketSegment, ELXMarketSegmentDAO} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXMarketSegmentDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXMarketSegmentDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxmarketsegment-dao-component',
    templateUrl: './ELXmarketsegmentdao.component.html'
})
export class ELXMarketSegmentComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    dataSource: DataSource = new DataSource(new ELXMarketSegmentDAOStore());

    ngOnInit() {
        this.initialize();
    }
}
