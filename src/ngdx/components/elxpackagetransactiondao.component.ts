/**
 * Component store DX: ELXPackageTransactionDAOStore
 * Description:  CRUD tabela - PackageTransaction
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXPackageTransaction, ELXPackageTransactionDAO, ELXMarketSegmentDD} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXPackageTransactionDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXPackageTransactionDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxpackagetransaction-dao-component',
    templateUrl: './ELXpackagetransactiondao.component.html'
})
export class ELXPackageTransactionComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    domainIdMarketSegment = new VSDomainDataStore(ELXMarketSegmentDD, 'id', 'name');
    dataSource: DataSource = new DataSource(new ELXPackageTransactionDAOStore());
    domains = [this.domainIdMarketSegment];

    ngOnInit() {
        this.initialize();
    }
}
