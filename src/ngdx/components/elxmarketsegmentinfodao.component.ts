/**
 * Component store DX: ELXMarketSegmentInfoDAOStore
 * Description:  CRUD tabela - MarketSegmentInfo
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXMarketSegmentInfo, ELXMarketSegmentInfoDAO, ELXMarketSegmentDD, ELXLanguageDD} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXMarketSegmentInfoDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['idMarketSegment', 'iDLanguage'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXMarketSegmentInfoDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxmarketsegmentinfo-dao-component',
    templateUrl: './ELXmarketsegmentinfodao.component.html'
})
export class ELXMarketSegmentInfoComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    domainIdMarketSegment = new VSDomainDataStore(ELXMarketSegmentDD, '<fill key>', '<fill value>');
    domainIDLanguage = new VSDomainDataStore(ELXLanguageDD, '<fill key>', '<fill value>');
    dataSource: DataSource = new DataSource(new ELXMarketSegmentInfoDAOStore());
    domains = [this.domainIdMarketSegment, this.domainIDLanguage];

    ngOnInit() {
        this.initialize();
    }
}
