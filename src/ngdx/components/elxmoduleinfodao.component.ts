/**
 * Component store DX: ELXModuleInfoDAOStore
 * Description:  CRUD tabela - ModuleInfo
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXModuleInfo, ELXModuleInfoDAO, ELXModuleDD, ELXLanguageDD} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXModuleInfoDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['idModule', 'iDLanguage'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXModuleInfoDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxmoduleinfo-dao-component',
    templateUrl: './ELXmoduleinfodao.component.html'
})
export class ELXModuleInfoComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    domainIdModule = new VSDomainDataStore(ELXModuleDD, '<fill key>', '<fill value>');
    domainIDLanguage = new VSDomainDataStore(ELXLanguageDD, '<fill key>', '<fill value>');
    dataSource: DataSource = new DataSource(new ELXModuleInfoDAOStore());
    domains = [this.domainIdModule, this.domainIDLanguage];

    ngOnInit() {
        this.initialize();
    }
}
