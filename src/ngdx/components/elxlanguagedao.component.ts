/**
 * Component store DX: ELXLanguageDAOStore
 * Description:  CRUD tabela - Language
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXLanguage, ELXLanguageDAO} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXLanguageDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXLanguageDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxlanguage-dao-component',
    templateUrl: './ELXlanguagedao.component.html'
})
export class ELXLanguageComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    dataSource: DataSource = new DataSource(new ELXLanguageDAOStore());

    ngOnInit() {
        this.initialize();
    }
}
