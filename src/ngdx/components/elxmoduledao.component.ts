/**
 * Component store DX: ELXModuleDAOStore
 * Description:  CRUD tabela - Module
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXModule, ELXModuleDAO} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXModuleDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXModuleDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxmodule-dao-component',
    templateUrl: './ELXmoduledao.component.html'
})
export class ELXModuleComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    dataSource: DataSource = new DataSource(new ELXModuleDAOStore());

    ngOnInit() {
        this.initialize();
    }
}
