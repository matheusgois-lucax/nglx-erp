/**
 * Component store DX: ELXBusinessOrganizationInfoDAOStore
 * Description:  CRUD tabela - BusinessOrganizationInfo
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { DxDataGridComponent } from 'devextreme-angular';
import { ELXBusinessOrganizationInfo, ELXBusinessOrganizationInfoDAO, ELXBusinessOrganizationDD, ELXLanguageDD} from '../vset/lucax_erplucax';
import { VSDomainDataStore, VSDataStore, VSDXGridComponent } from 'vset-ngdx';

export class ELXBusinessOrganizationInfoDAOStore extends VSDataStore {
    protected readonly keys: Array<string> = ['idBusinessOrganization', 'iDLanguage'];
    protected readonly aiKeys: Array<string> = ['id'];
    protected readonly metaVSClass: any = ELXBusinessOrganizationInfoDAO;
}

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'elxbusinessorganizationinfo-dao-component',
    templateUrl: './ELXbusinessorganizationinfodao.component.html'
})
export class ELXBusinessOrganizationInfoComponent extends VSDXGridComponent implements OnInit {
    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    domainIdBusinessOrganization = new VSDomainDataStore(ELXBusinessOrganizationDD, '<fill key>', '<fill value>');
    domainIDLanguage = new VSDomainDataStore(ELXLanguageDD, '<fill key>', '<fill value>');
    dataSource: DataSource = new DataSource(new ELXBusinessOrganizationInfoDAOStore());
    domains = [this.domainIdBusinessOrganization, this.domainIDLanguage];

    ngOnInit() {
        this.initialize();
    }
}
